package preprocessing;

public class Tile {

    private final int x; // x position in the grid
    private final int y; // y position in the grid
    private final int w; // grid width
    private final int h; // grid height
    private final float startXPerc; // x-Percentage of the original video where the tiling must begin
    private final float startYPerc; // y-Percentage of the original video where the tiling must begin
    private final float endXPerc; // x-Percentage of the original video where the tiling must end
    private final float endYPerc; // y-Percentage of the original video where the tiling must end

    public Tile(int x, int y, int w, int h, float startXPerc, float startYPerc, float endXPerc, float endYPerc) {
        if (x < 0 || y < 0 || w <= 0 || h <= 0) {
            throw new IllegalArgumentException("Invalid tile parameters: x=" + x + ", y=" + y + ", w=" + w + ", h=" + h);
        }
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.startXPerc = startXPerc;
        this.startYPerc = startYPerc;
        this.endXPerc = endXPerc;
        this.endYPerc = endYPerc;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public String generateCommandPortion(int width, int height) {
        int startWpixel = (int) (width * startXPerc);
        int startHpixel = (int) (height * startYPerc);
        int endWpixel = (int) (width * endXPerc);
        int endHpixel = (int) (height * endYPerc);
        int areaWidth = endWpixel - startWpixel;
        int areaHeight = endHpixel - startHpixel;

        // Debug statements to trace values
        System.out.println("Tile dimensions calculated: ");
        System.out.println("startWpixel: " + startWpixel + ", startHpixel: " + startHpixel);
        System.out.println("endWpixel: " + endWpixel + ", endHpixel: " + endHpixel);
        System.out.println("areaWidth: " + areaWidth + ", areaHeight: " + areaHeight);

        if (areaWidth <= 0 || areaHeight <= 0) {
            throw new IllegalArgumentException("Invalid tile dimensions: width=" + areaWidth + ", height=" + areaHeight);
        }

        return "crop=" + areaWidth + ":" + areaHeight + ":" + startWpixel + ":" + startHpixel;
    }

    public String generateLog() {
        return "\t\tGenerating the tile with position (" + x + "," + y + ") and size (" + w + "," + h + ")";
    }

    public String generateFileSuffix() {
        return "-" + x + "" + y + "" + w + "" + h;
    }

    public String generateSRDProperty() {
        return x + "," + y + "," + w + "," + h;
    }
}
