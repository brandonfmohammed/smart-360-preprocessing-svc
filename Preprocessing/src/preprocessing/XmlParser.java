package preprocessing;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class XmlParser {
    public static Worklist parse(String filePath) {
        try {
            File xmlFile = new File(filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            Worklist worklist = new Worklist();

            NodeList pathNodes = doc.getElementsByTagName("path");
            if (pathNodes.getLength() == 0 || pathNodes.item(0) == null) {
                throw new NullPointerException("videoPath element is missing in XML file");
            }
            worklist.setVideoPath(pathNodes.item(0).getTextContent());

            NodeList outputNodes = doc.getElementsByTagName("output");
            if (outputNodes.getLength() == 0 || outputNodes.item(0) == null) {
                throw new NullPointerException("output element is missing in XML file");
            }
            worklist.setOutput(outputNodes.item(0).getTextContent());

            // Parse SVC layers
            NodeList svcNodes = doc.getElementsByTagName("SVC");
            if (svcNodes.getLength() > 0 && svcNodes.item(0) != null) {
                NodeList baseLayerNodes = doc.getElementsByTagName("baseLayer");
                NodeList enhancementLayer1Nodes = doc.getElementsByTagName("enhancementLayer1");
                NodeList enhancementLayer2Nodes = doc.getElementsByTagName("enhancementLayer2");
                NodeList enhancementLayer3Nodes = doc.getElementsByTagName("enhancementLayer3");

                if (baseLayerNodes.getLength() > 0 && baseLayerNodes.item(0) != null) {
                    int baseLayer = Integer.parseInt(baseLayerNodes.item(0).getTextContent());
                    int enhancementLayer1 = Integer.parseInt(enhancementLayer1Nodes.item(0).getTextContent());
                    int enhancementLayer2 = Integer.parseInt(enhancementLayer2Nodes.item(0).getTextContent());
                    int enhancementLayer3 = Integer.parseInt(enhancementLayer3Nodes.item(0).getTextContent());
                    worklist.addVersion(new Version(baseLayer, enhancementLayer1, enhancementLayer2, enhancementLayer3));
                }
            }

            // Parse tiling configurations
            NodeList tilingNodes = doc.getElementsByTagName("tiling");
            for (int i = 0; i < tilingNodes.getLength(); i++) {
                if (tilingNodes.item(i) != null) {
                    String[] dimensions = tilingNodes.item(i).getTextContent().split("x");
                    int hTiles = Integer.parseInt(dimensions[0]);
                    int vTiles = Integer.parseInt(dimensions[1]);
                    worklist.addTiling(new Tiling(hTiles, vTiles));
                }
            }

            // Parse segment lengths
            NodeList segmentNodes = doc.getElementsByTagName("segment");
            for (int i = 0; i < segmentNodes.getLength(); i++) {
                if (segmentNodes.item(i) != null) {
                    int segment = Integer.parseInt(segmentNodes.item(i).getTextContent());
                    worklist.addSegment(segment);
                }
            }

            return worklist;
        } catch (Exception e) {
            throw new RuntimeException("Error parsing XML file: " + filePath, e);
        }
    }
}
