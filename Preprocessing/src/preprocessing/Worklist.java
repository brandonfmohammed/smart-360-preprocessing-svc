package preprocessing;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Worklist {
    private final List<Version> versions;
    private final List<Tiling> tilings;
    private final List<Integer> segments;
    private String videoPath;
    private String currentVideoPath;
    private int videoWidth;
    private int videoHeight;
    private int videoFPS;
    private String output;
    private String outputName;

    public Worklist() {
        this.versions = new ArrayList<>();
        this.tilings = new ArrayList<>();
        this.segments = new ArrayList<>();
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public void addVersion(Version version) {
        versions.add(version);
    }

    public void addTiling(Tiling tiling) {
        tilings.add(tiling);
    }

    public void addSegment(int segment) {
        segments.add(segment);
    }

    public void videoExists() throws FileNotFoundException {
        File f = new File(currentVideoPath);
        if (!f.exists() || f.isDirectory())
            throw new FileNotFoundException("The video file provided doesn't exist!");
    }

    public void outputDirectory() throws SecurityException, IOException {
        File root = new File(output);
        outputName = root.getName();
        if (root.exists()) {
            Files.walk(root.toPath())
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        Files.createDirectories(root.toPath());

        for (Tiling tiling : tilings) {
            String outputTiling = String.format("%s/%dx%d", output, tiling.gethTiles(), tiling.getvTiles());
            File outputTilingFile = new File(outputTiling);
            if (outputTilingFile.exists()) {
                Files.walk(outputTilingFile.toPath())
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
            }
            Files.createDirectories(outputTilingFile.toPath());

            for (int segment : segments) {
                String outputSegment = String.format("%s/%d", outputTiling, segment);
                File outputSegmentFile = new File(outputSegment);
                if (outputSegmentFile.exists()) {
                    Files.walk(outputSegmentFile.toPath())
                            .sorted(Comparator.reverseOrder())
                            .map(Path::toFile)
                            .forEach(File::delete);
                }
                Files.createDirectories(outputSegmentFile.toPath());
            }
        }
    }

    public void videoDimensions() throws IOException, InterruptedException {
        Runtime runTime = Runtime.getRuntime();

        String widthCommand = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=width " + currentVideoPath;
        String heightCommand = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=height " + currentVideoPath;
        String fpsCommand = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=avg_frame_rate " + currentVideoPath;

        // Width
        Process widthProcess = runTime.exec(widthCommand);
        ProcessGetDimensions widthOutput = new ProcessGetDimensions(widthProcess.getInputStream());
        widthOutput.start();
        widthProcess.waitFor();
        String widthStr = widthOutput.returnValue();
        System.out.println("Width output: " + widthStr);
        if (widthStr == null || widthStr.isEmpty()) {
            throw new IOException("Failed to retrieve width using ffprobe.");
        }
        videoWidth = Integer.parseInt(widthStr);
        widthProcess.destroy();

        // Height
        Process heightProcess = runTime.exec(heightCommand);
        ProcessGetDimensions heightOutput = new ProcessGetDimensions(heightProcess.getInputStream());
        heightOutput.start();
        heightProcess.waitFor();
        String heightStr = heightOutput.returnValue();
        System.out.println("Height output: " + heightStr);
        if (heightStr == null || heightStr.isEmpty()) {
            throw new IOException("Failed to retrieve height using ffprobe.");
        }
        videoHeight = Integer.parseInt(heightStr);
        heightProcess.destroy();

        // FPS
        Process fpsProcess = runTime.exec(fpsCommand);
        ProcessGetDimensions fpsOutput = new ProcessGetDimensions(fpsProcess.getInputStream());
        fpsOutput.start();
        fpsProcess.waitFor();
        String fpsStr = fpsOutput.returnValue();
        System.out.println("FPS output: " + fpsStr);
        if (fpsStr == null || fpsStr.isEmpty()) {
            throw new IOException("Failed to retrieve fps using ffprobe.");
        }
        videoFPS = parseFPS(fpsStr.replace("\"", "").trim()); // Remove any surrounding quotes and trim spaces
        fpsProcess.destroy();
    }

public void tile() throws IOException, InterruptedException {
    for (Tiling currentTiling : tilings) {
        String tilingDir = String.format("%s/%dx%d", output, currentTiling.gethTiles(), currentTiling.getvTiles());
        Files.createDirectories(Path.of(tilingDir));

        List<Long> segmentSizes = new ArrayList<>(); // List to hold segment sizes

        for (Tile tile : currentTiling.getTiles()) {
            String tileCropCommand = tile.generateCommandPortion(videoWidth, videoHeight);
            if (tileCropCommand.contains("crop=0:0")) {
                throw new IllegalArgumentException("Invalid tile dimensions generated: " + tileCropCommand);
            }

            int tile_x = tile.getX();
            int tile_y = tile.getY();
            int w = currentTiling.gethTiles();
            int h = currentTiling.getvTiles();

            // Additional debug logging
            System.out.println("Processing tile: " + tile);
            System.out.println("tile_x: " + tile_x + ", tile_y: " + tile_y + ", w: " + w + ", h: " + h);

            // Validation check
            if (tile_x >= w || tile_y >= h || tile_x < 0 || tile_y < 0) {
                System.out.println("Invalid tile coordinates for tile " + tile + ": (tile_x=" + tile_x + ", tile_y=" + tile_y + ") with dimensions (w=" + w + ", h=" + h + ")");
                continue; // Skip this tile if the coordinates are invalid
            }

            // Define output filenames for base and enhancement layers
            String baseLayerOutput = String.format("%s/%dx%d_tile_%d_%d_base_%%03d.mp4", tilingDir, currentTiling.gethTiles(), currentTiling.getvTiles(), tile_x, tile_y);
            String enhancementLayer1Output = String.format("%s/%dx%d_tile_%d_%d_enhancement1_%%03d.mp4", tilingDir, currentTiling.gethTiles(), currentTiling.getvTiles(), tile_x, tile_y);
            String enhancementLayer2Output = String.format("%s/%dx%d_tile_%d_%d_enhancement2_%%03d.mp4", tilingDir, currentTiling.gethTiles(), currentTiling.getvTiles(), tile_x, tile_y);
            String enhancementLayer3Output = String.format("%s/%dx%d_tile_%d_%d_enhancement3_%%03d.mp4", tilingDir, currentTiling.gethTiles(), currentTiling.getvTiles(), tile_x, tile_y);

            // Define encoding commands for each layer
            String commandBaseLayer = String.format(
                "ffmpeg -i %s -vf \"%s,scale=iw*2:ih*2,pad=iw:ih:(iw-iw/2):(ih-ih/2)\" -c:v libx264 -preset slow -b:v 1500k -sc_threshold 0 -force_key_frames 'expr:gte(t,2)' " +
                "-x264-params \"scenecut=0:keyint=50:min-keyint=50:open-gop=0\" -f segment -segment_time 2 -reset_timestamps 1 -map 0 %s",
                currentVideoPath, tileCropCommand.trim(), baseLayerOutput
            );

            String commandEnhancementLayer1 = String.format(
                "ffmpeg -i %s -vf \"%s,scale=iw*2:ih*2,pad=iw:ih:(iw-iw/2):(ih-ih/2)\" -c:v libx264 -preset slow -b:v 1000k -sc_threshold 0 -force_key_frames 'expr:gte(t,2)' " +
                "-x264-params \"scenecut=0:keyint=50:min-keyint=50:open-gop=0\" -f segment -segment_time 2 -reset_timestamps 1 -map 0 %s",
                currentVideoPath, tileCropCommand.trim(), enhancementLayer1Output
            );

            String commandEnhancementLayer2 = String.format(
                "ffmpeg -i %s -vf \"%s,scale=iw*2:ih*2,pad=iw:ih:(iw-iw/2):(ih-ih/2)\" -c:v libx264 -preset slow -b:v 700k -sc_threshold 0 -force_key_frames 'expr:gte(t,2)' " +
                "-x264-params \"scenecut=0:keyint=50:min-keyint=50:open-gop=0\" -f segment -segment_time 2 -reset_timestamps 1 -map 0 %s",
                currentVideoPath, tileCropCommand.trim(), enhancementLayer2Output
            );

            String commandEnhancementLayer3 = String.format(
                "ffmpeg -i %s -vf \"%s,scale=iw*2:ih*2,pad=iw:ih:(iw-iw/2):(ih-ih/2)\" -c:v libx264 -preset slow -b:v 500k -sc_threshold 0 -force_key_frames 'expr:gte(t,2)' " +
                "-x264-params \"scenecut=0:keyint=50:min-keyint=50:open-gop=0\" -f segment -segment_time 2 -reset_timestamps 1 -map 0 %s",
                currentVideoPath, tileCropCommand.trim(), enhancementLayer3Output
            );

            System.out.println("Executing FFmpeg command for base layer: " + commandBaseLayer);
            executeCommand(commandBaseLayer, segmentSizes); // Pass the list to collect segment sizes
            System.out.println("Executing FFmpeg command for enhancement layer 1: " + commandEnhancementLayer1);
            executeCommand(commandEnhancementLayer1, segmentSizes); // Pass the list to collect segment sizes
            System.out.println("Executing FFmpeg command for enhancement layer 2: " + commandEnhancementLayer2);
            executeCommand(commandEnhancementLayer2, segmentSizes); // Pass the list to collect segment sizes
            System.out.println("Executing FFmpeg command for enhancement layer 3: " + commandEnhancementLayer3);
            executeCommand(commandEnhancementLayer3, segmentSizes); // Pass the list to collect segment sizes
        }

        // Log segment sizes for the current tiling
        System.out.println("Segment sizes for tiling " + currentTiling.gethTiles() + "x" + currentTiling.getvTiles() + ": " + segmentSizes);
    }
}

    private void executeCommand(String command, List<Long> segmentSizes) throws IOException, InterruptedException {
        System.out.println("Command: " + command);

        ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash", "-c", command);
        processBuilder.redirectErrorStream(true);
        Process process = processBuilder.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            // Extract segment size information from FFmpeg output if available
            if (line.contains("size=")) {
                String sizeStr = line.split("size=")[1].trim().split(" ")[0];
                try {
                    long size = Long.parseLong(sizeStr);
                    segmentSizes.add(size);
                } catch (NumberFormatException e) {
                    // Handle parsing error
                }
            }
        }
        process.waitFor();
    }

public void segment() throws IOException, InterruptedException {
    for (Tiling currentTiling : tilings) {
        for (int segment : segments) {
            String segmentDir = String.format("%s/%dx%d/%d", output, currentTiling.gethTiles(), currentTiling.getvTiles(), segment);
            Files.createDirectories(Path.of(segmentDir));
            System.out.printf("Creating segment directory: %s%n", segmentDir);

            Runtime runTime = Runtime.getRuntime();
            Process worker;
            ProcessOutputStream outputStream;
            StringBuilder command = new StringBuilder("MP4Box -dash " + segment + " -segment-name tile_ -rap -out " + segmentDir + "/manifest.mpd ");

            for (Tile tile : currentTiling.getTiles()) {
                // Base layer
                appendFilesToCommand(command, "base", tile, currentTiling);

                // Enhancement layers
                appendFilesToCommand(command, "enhancement1", tile, currentTiling);
                appendFilesToCommand(command, "enhancement2", tile, currentTiling);
                appendFilesToCommand(command, "enhancement3", tile, currentTiling);
            }

            String finalCommand = command.toString();
            System.out.println("Executing MP4Box command: " + finalCommand); // Debugging statement

            // Create a temporary script file
            String tempScriptPath = segmentDir + "/dash.sh";
            PrintStream writer = new PrintStream(new FileOutputStream(tempScriptPath));
            writer.println(finalCommand);
            writer.close();

            // Set execute permissions for the script
            worker = runTime.exec("chmod 777 " + tempScriptPath);
            outputStream = new ProcessOutputStream(worker.getErrorStream());
            outputStream.start();
            worker.waitFor();
            worker.destroy();

            // Execute the script
            worker = runTime.exec(tempScriptPath);
            outputStream = new ProcessOutputStream(worker.getErrorStream());
            outputStream.start();
            worker.waitFor();
            outputStream.join();
            worker.destroy();
        }
    }
}


private void appendFilesToCommand(StringBuilder command, String layer, Tile tile, Tiling currentTiling) {
    for (int i = 0; i <= 9; i++) { // Adjust range based on actual segments
        String filePath = String.format(
            "%s/%dx%d/12x6_tile_%d_%d_%s_%03d.mp4#video:desc_as='<SupplementalProperty schemeIdUri=\"urn:mpeg:dash:srd:2014\" value=\"%d,%d,0,%d,%d,12,6\"/>'",
            output, currentTiling.gethTiles(), currentTiling.getvTiles(), tile.getX(), tile.getY(), layer, i, tile.getX(), tile.getY(), currentTiling.gethTiles(), currentTiling.getvTiles()
        );
        command.append(filePath).append(" ");
    }
}


    private int parseFPS(String fracFPS) {
        if (fracFPS.contains("/")) {
            String[] ratio = fracFPS.split("/");
            return (int) Math.round(Double.parseDouble(ratio[0]) / Double.parseDouble(ratio[1]));
        } else {
            return (int) Math.round(Double.parseDouble(fracFPS));
        }
    }

    public void startProcessing() throws IOException, InterruptedException {
        outputDirectory();
        Path videoDirectoryPath = Paths.get(videoPath);
        if (Files.isDirectory(videoDirectoryPath)) {
            List<Path> videoFiles = Files.list(videoDirectoryPath)
                    .filter(path -> path.toString().endsWith(".mp4"))
                    .collect(Collectors.toList());

            for (Path videoFilePath : videoFiles) {
                currentVideoPath = videoFilePath.toString();
                videoExists();
                videoDimensions();
                tile();
                segment();
            }
        } else {
            currentVideoPath = videoPath;
            videoExists();
            videoDimensions();
            tile();
            segment();
        }

        // Validate segment filenames
        validateSegmentFilenames();
    }

    private void validateSegmentFilenames() {
        File outputDirectory = new File(output);
        File[] files = outputDirectory.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    processFile(file);
                }
            }
        }
    }

    private void processFile(File file) {
        // Regular expression to parse the filename
        Pattern pattern = Pattern.compile("tile_(\\d+)_(\\d+)\\.m4s");
        Matcher matcher = pattern.matcher(file.getName());

        if (matcher.matches()) {
            int tile_id = Integer.parseInt(matcher.group(1));
            int segment = Integer.parseInt(matcher.group(2));

            // Assuming w and h are fixed values for this example
            int w = 12;
            int h = 6;

            // Calculate tile_x and tile_y
            int tile_x = (tile_id % w);
            int tile_y = (tile_id / w);

            // Log the values
            System.out.println("Processing file: " + file.getName());
            System.out.println("tile_id: " + tile_id + ", w: " + w + ", h: " + h + ", tile_x: " + tile_x + ", tile_y: " + tile_y);

            // Validate the coordinates
            if (tile_x >= w || tile_y >= h) {
                System.err.println("Invalid tile coordinates for file " + file.getName() + ": (tile_x=" + tile_x + ", tile_y=" + tile_y + ") with dimensions (w=" + w + ", h=" + h + ")");
            } else {
                // Process the file if the coordinates are valid
                int size = getFileSizeInBits(file);
                System.out.println("File: " + file.getName() + ", Tile X: " + tile_x + ", Tile Y: " + tile_y + ", Segment: " + segment + ", Layer: base, Size: " + size + " bits");
            }
        } else {
            System.err.println("Filename does not match expected pattern: " + file.getName());
        }
    }

    private int getFileSizeInBits(File file) {
        // Placeholder method to get the file size in bits
        return (int) (file.length() * 8);
    }
}
