package preprocessing;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Preprocess {
    public static void main(String[] args) throws Exception {
        System.out.println("Preliminary actions... completed!");
        System.out.println("Parsing the XML files...");

        List<Worklist> worklists = Stream.of(args)
                .flatMap(xmlFile -> {
                    try {
                        System.out.println("Parsing XML file: " + xmlFile);
                        return Stream.of(XmlParser.parse(xmlFile));
                    } catch (Exception e) {
                        System.err.println("Error parsing XML file: " + xmlFile);
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());

        for (Worklist worklist : worklists) {
            worklist.startProcessing();
        }

        System.out.println("Process completed!");
    }
}
