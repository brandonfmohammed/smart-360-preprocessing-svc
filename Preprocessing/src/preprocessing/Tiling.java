package preprocessing;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Tiling {

    private final int hTiles;
    private final int vTiles;
    private final List<Tile> tiles;

    public Tiling(int hTiles, int vTiles) {
        if (hTiles <= 0 || vTiles <= 0) {
            throw new IllegalArgumentException("Invalid tiling dimensions: hTiles=" + hTiles + ", vTiles=" + vTiles);
        }
        this.hTiles = hTiles;
        this.vTiles = vTiles;
        this.tiles = new ArrayList<>();

        for (int i = 0; i < hTiles; i++) {
            for (int j = 0; j < vTiles; j++) {
                float startXPerc = BigDecimal.valueOf(i / (double) hTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                float startYPerc = BigDecimal.valueOf(j / (double) vTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                float endXPerc = BigDecimal.valueOf((i + 1) / (double) hTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                float endYPerc = BigDecimal.valueOf((j + 1) / (double) vTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                tiles.add(new Tile(i, j, 1, 1, startXPerc, startYPerc, endXPerc, endYPerc));
            }
        }

        // Debugging: Log the tiling grid
        System.out.println("Tiling grid created with dimensions: " + hTiles + "x" + vTiles);
        for (Tile tile : tiles) {
            System.out.println(tile.generateLog());
        }
    }

    public int gethTiles() {
        return hTiles;
    }

    public int getvTiles() {
        return vTiles;
    }

    public List<Tile> getTiles() {
        return tiles;
    }
}
