/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

/**
 * Class of constants.
 * It contains useful parameters used to parse the XML file and to crop videos.
 */

public abstract class K {
    // XML tags
    public static final int BASE_SVC = 28; // SVC for base layer
    public static final int ENH1_SVC = 23; // SVC for enhancement layer 1
    public static final int ENH2_SVC = 18; // SVC for enhancement layer 2
    public static final int ENH3_SVC = 15; // SVC for enhancement layer 3
    public static final String videoTag = "video";
    public static final String videoPathTag = "path";
    public static final String videoSVCsTag = "SVCs";
    public static final String SVCTag = "SVC";
    public static final String baseLayerTag = "base_layer";
    public static final String enhancementLayer1Tag = "enhancement_layer1";
    public static final String enhancementLayer2Tag = "enhancement_layer2";
    public static final String enhancementLayer3Tag = "enhancement_layer3";
    public static final String videoTilingsTag = "tilings";
    public static final String tilingTag = "tiling";
    public static final String videoSegmentsTag = "segments";
    public static final String segmentTag = "segment";
    public static final String outputTag = "output";
}
