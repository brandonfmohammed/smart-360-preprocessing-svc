/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

public class Version {

    private final int baseLayer;
    private final int enh1Layer;
    private final int enh2Layer;
    private final int enh3Layer;

    public Version(int baseLayer, int enh1Layer, int enh2Layer, int enh3Layer) {
        this.baseLayer = baseLayer;
        this.enh1Layer = enh1Layer;
        this.enh2Layer = enh2Layer;
        this.enh3Layer = enh3Layer;
    }

    public String generateCommandPortion(int framerate, String layerType) {
        switch (layerType) {
            case "base":
                return " -c:v libx264 -preset medium -x264-params \"level=3.0:scenecut=0:keyint=" + framerate + ":min-keyint=" + framerate + ":no-scenecut=1:force-cfr=1\" ";
            case "enh1":
                return " -c:v libx264 -preset medium -x264-params \"level=3.1:scenecut=0:keyint=" + framerate + ":min-keyint=" + framerate + ":no-scenecut=1:force-cfr=1\" ";
            case "enh2":
                return " -c:v libx264 -preset medium -x264-params \"level=3.2:scenecut=0:keyint=" + framerate + ":min-keyint=" + framerate + ":no-scenecut=1:force-cfr=1\" ";
            case "enh3":
                return " -c:v libx264 -preset medium -x264-params \"level=3.3:scenecut=0:keyint=" + framerate + ":min-keyint=" + framerate + ":no-scenecut=1:force-cfr=1\" ";
            default:
                throw new IllegalArgumentException("Invalid layer type: " + layerType);
        }
    }

    public String generateFileSuffix(String layerType) {
        return "-" + layerType;
    }

    public int getBaseLayer() {
        return baseLayer;
    }

    public int getEnh1Layer() {
        return enh1Layer;
    }

    public int getEnh2Layer() {
        return enh2Layer;
    }

    public int getEnh3Layer() {
        return enh3Layer;
    }
}
