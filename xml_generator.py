import os
import xml.etree.ElementTree as ET

parameters = {
    'input_videos': tuple([f'./source/videos/{f.strip()}' for f in open('./source/videos/test_videos', 'r')]),
    'base_layer': '28',
    'enhancement_layers': ('23', '18', '15'),  # Three enhancement layers
    'tile_layouts': ('12x6', ),
    'segment_lengths': (1, )  # Segment length in seconds
}

if not os.path.exists('./xml/'):
    os.makedirs('./xml/')

for input_video in parameters['input_videos']:
    video = ET.Element('video')

    path = ET.SubElement(video, 'path')
    path.text = input_video

    SVCs = ET.SubElement(video, 'SVCs')
    SVC = ET.SubElement(SVCs, 'SVC')

    base_layer = ET.SubElement(SVC, 'baseLayer')
    base_layer.text = parameters['base_layer']

    enhancements = ET.SubElement(SVC, 'enhancementLayers')
    for i, layer in enumerate(parameters['enhancement_layers']):
        enhancement_layer = ET.SubElement(SVC, f'enhancementLayer{i+1}')
        enhancement_layer.text = layer

    tilings = ET.SubElement(video, 'tilings')
    for tile_layout in parameters['tile_layouts']:
        tiling = ET.SubElement(tilings, 'tiling')
        tiling.text = tile_layout

    segments = ET.SubElement(video, 'segments')
    for segment_length in parameters['segment_lengths']:
        segment = ET.SubElement(segments, 'segment')
        segment.text = str(segment_length * 1000)  # Segment length in milliseconds

    output = ET.SubElement(video, 'output')
    output.text = f'./output/{os.path.basename(input_video).split(".")[0]}'

    tree = ET.ElementTree(video)
    tree.write(f'./xml/{os.path.basename(input_video).split(".")[0]}.xml', encoding='utf-8', xml_declaration=True)
