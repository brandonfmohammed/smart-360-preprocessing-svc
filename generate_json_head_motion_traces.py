#
# SMART360 Preprocessing
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json
import os
from pathlib import Path

from tqdm.auto import tqdm

DATASETS = ['./source/head_motion_traces/David_MMSys_18',
            './source/head_motion_traces/Xu_CVPR_18',
            './source/head_motion_traces/Xu_PAMI_18']
USER_JSON_DST_DIR = './json_output/head_motion_traces'

for dataset in DATASETS:
    for video in tqdm(sorted(os.listdir(dataset))):
        Path(f'{USER_JSON_DST_DIR}/{video}/').mkdir(parents=True, exist_ok=True)
        for user in sorted(os.listdir(f'{dataset}/{video}')):
            json_user_file = f'{USER_JSON_DST_DIR}/{video}/{user}.json'
            json_content = []
            trace = open(f'{dataset}/{video}/{user}', 'r')
            for line in trace:
                time_sec, x, y, z = map(float, line.split(','))
                json_content.append({'time_ms': int(time_sec * 1000), 'coordinates': [x, y, z]})
            json.dump(json_content, open(json_user_file, 'w'))
