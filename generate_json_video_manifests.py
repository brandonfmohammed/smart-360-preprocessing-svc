import math
import json
import os
from pathlib import Path
import subprocess
import re

import numpy as np
from tqdm.auto import tqdm

SOURCE_VIDEO_DIR = './source/videos'
OUTPUT_ROOT_DIR = './output'
VIDEO_JSON_DST_DIR = './json_output/video_manifests'
N_QUALITIES = 4

def tile_id_to_xy(tile_id, w):
    tile_x = tile_id % w
    tile_y = tile_id // w
    return tile_x, tile_y

def extract_tile_info(filename):
    pattern = re.compile(r"tile__r(\d+)_(\d+)\.m4s")
    match = pattern.match(filename)
    if match:
        tile_id = int(match.group(1))
        segment_number = int(match.group(2))
        return tile_id, segment_number
    else:
        print(f"Skipping file {filename} due to error: filename does not match expected pattern")
        return None

def ensure_list_size(lst, size, default_value):
    while len(lst) < size:
        lst.append(default_value)

def remove_empty_lists(lst):
    if isinstance(lst, list):
        return [remove_empty_lists(x) for x in lst if x or x == 0]
    else:
        return lst

def flatten(lst):
    """Flatten a list of lists"""
    for item in lst:
        if isinstance(item, list):
            for subitem in flatten(item):
                yield subitem
        else:
            yield item

print("Starting JSON manifest generation...")

for video in tqdm(sorted(os.listdir(OUTPUT_ROOT_DIR))):
    if video == 'old':
        continue
    video_duration = float(subprocess.run(['ffprobe', '-i', f'{SOURCE_VIDEO_DIR}/{video}.mp4', '-show_entries',
                                           'format=duration', '-v', 'quiet', '-of', 'csv=p=0'], stdout=subprocess.PIPE)
                           .stdout.decode()[:-1])
    Path(f'{VIDEO_JSON_DST_DIR}/{video}/').mkdir(parents=True, exist_ok=True)
    video_path = os.path.join(OUTPUT_ROOT_DIR, video)
    for tiling in os.listdir(video_path):
        w, h = map(int, tiling.split('x'))
        tiling_path = os.path.join(video_path, tiling)
        for segment in os.listdir(tiling_path):
            try:
                segment = int(segment)
            except ValueError:
                continue
            segment_path = os.path.join(tiling_path, str(segment))
            json_video_file = f'{VIDEO_JSON_DST_DIR}/{video}/{tiling}_{segment}.json'
            json_content = {
                'segment_duration_ms': 1000,
                'tiles_x': w,
                'tiles_y': h,
                'bitrates_kbps': [],
                '_comment_': 'segment_sizes_bits[segment_index][tile_index][base|enh1|enh2|enh3]',
                'segment_sizes_bits': []
            }
            segment_sizes_bits = []
            for f in os.listdir(segment_path):
                if f[-4:] != '.m4s':
                    continue
                print(f"Processing file: {f}")
                tile_info = extract_tile_info(f)
                if tile_info:
                    tile_id, segment_number = tile_info
                    tile_x, tile_y = tile_id_to_xy(tile_id, w)
                    print(f"tile_id: {tile_id}, w: {w}, h: {h}, tile_x: {tile_x}, tile_y: {tile_y}")
                    
                    layer_map = {'base': 0, 'enh1': 1, 'enh2': 2, 'enh3': 3}
                    layer = 'base'  # Default to base layer
                    parts = f.split('_')
                    if len(parts) > 3:
                        layer_candidate = parts[-2]
                        if layer_candidate in layer_map:
                            layer = layer_candidate
                    file_path = os.path.join(segment_path, f)
                    file_size = os.path.getsize(file_path) * 8
                    print(f"File: {f}, Tile X: {tile_x}, Tile Y: {tile_y}, Segment: {segment_number}, Layer: {layer}, Size: {file_size} bits")
                    
                    # Ensure segment_sizes_bits is large enough to hold this data
                    while len(segment_sizes_bits) <= segment_number:
                        segment_sizes_bits.append([[] for _ in range(w * h)])
                    while len(segment_sizes_bits[segment_number]) <= tile_y * w + tile_x:
                        segment_sizes_bits[segment_number].append([0] * N_QUALITIES)
                    
                    if len(segment_sizes_bits[segment_number][tile_y * w + tile_x]) < N_QUALITIES:
                        segment_sizes_bits[segment_number][tile_y * w + tile_x] = [0] * N_QUALITIES

                    if layer in layer_map:
                        segment_sizes_bits[segment_number][tile_y * w + tile_x][layer_map[layer]] = file_size
            
            # Ensure uniform shape and remove empty lists
            segment_sizes_bits = remove_empty_lists(segment_sizes_bits)

            # Remove any empty lists left in the outer list
            segment_sizes_bits = [s for s in segment_sizes_bits if any(flatten(s))]
            
            print(f"segment_sizes_bits before final processing for segment {segment}: {segment_sizes_bits}")
            
            # Calculate bitrates_kbps and ensure homogeneous shape
            json_content['bitrates_kbps'] = (np.array(segment_sizes_bits, dtype=np.float64).sum(axis=(0, 1)) / (video_duration * 1000)).round().astype(int).tolist()
            json_content['segment_sizes_bits'] = segment_sizes_bits
            
            with open(json_video_file, 'w') as json_file:
                json.dump(json_content, json_file, indent=2)

print("JSON manifest generation completed.")
