# SMART360 Preprocessing
This repository contains tools for preprocessing data before it can be used as input by the SMART360 simulator.
This repository enables:
- the generation of simplified JSON video manifests that can be used by the SMART360 simulator from MP4 videos, representing DASH-SRD compliant tiles and segments;
- the generation of JSON user head motion traces that can be used by the SMART360 simulator from 360° head motion datasets, originally in CSV files.

## Citing this work
```
@inbook{10.1145/3587819.3592547,
author = {Guimard, Quentin and Sassatelli, Lucile},
title = {SMART360: Simulating Motion Prediction and Adaptive BitRate STrategies for 360° Video Streaming},
year = {2023},
isbn = {9798400701481},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3587819.3592547},
abstract = {Adaptive bitrate (ABR) algorithms are used in streaming media to adjust video or audio quality based on the viewer's network conditions to provide a smooth playback experience. With the rise of virtual reality (VR) headsets, 360° video streaming is growing rapidly and requires efficient ABR strategies to also adapt the video quality to the user's head position. However, research in this field is often difficult to compare due to a lack of reproducible simulations. To address this problem, we provide SMART360, a 360° streaming simulation environment to compare motion prediction and adaptive bitrates strategies. We provide sample inputs and baseline algorithms along with the simulator, as well as examples of results and visualizations that can be obtained with SMART360. The code and data are made publicly available.},
booktitle = {Proceedings of the 14th Conference on ACM Multimedia Systems},
pages = {384–390},
numpages = {7}
}
```

<img src="https://www.acm.org/binaries/content/gallery/acm/publications/artifact-review-v1_1-badges/artifacts_evaluated_functional_v1_1.png" alt="ACM badge: Artifacts Evaluated & Reusable / v1.1" width="100">

## Authors
- [Quentin Guimard](mailto:quentin.guimard@univ-cotedazur.fr) - Université Côte d'Azur, CNRS, I3S
- Lucile Sassatelli - Université Côte d'Azur, CNRS, I3S, Institut Universitaire de France

## Getting started
We recommend using Docker. We provide an [image](https://hub.docker.com/repository/docker/qguim/smart360-prep_env/general) with the required software and libraries installed.
Please refer to the [official documentation](https://docs.docker.com/engine/install/) to install Docker on your machine if it is not already installed.

Follow these instructions to set up the SMART360 preprocessing pipeline with Docker:
```shell
git clone https://gitlab.com/SMART360/SMART360-preprocessing.git /path/to/SMART360-preprocessing
chmod 777 /path/to/SMART360-preprocessing  # give file permissions to docker in SMART360 if the id does not match by default (docker uid/gid: 1000)
# Download the source files (link below) and place them in the /path/to/SMART360-preprocessing/source directory
docker pull qguim/smart360-prep_env:latest
docker run -d -it --rm -v /path/to/SMART360-preprocessing:/SMART360-preprocessing --name SMART360-preprocessing qguim/smart360-prep_env:latest
docker attach SMART360-preprocessing
cd /SMART360-preprocessing
python xml_generator.py  # generate XML video configurations (videos, quality levels, tile layouts, segment lengths) 
javac -d Preprocessing/bin/ Preprocessing/src/preprocessing/*.java  # compile preprocessing pipeline
java -classpath Preprocessing/bin/ preprocessing.Preprocess xml/*.xml  # execute preprocessing pipeline with FFMPEG tiling/re-encoding and DASH packaging in tracks and segments
python generate_json_video_manifests.py  # generate simulator-ready JSON video manifests
python generate_json_head_motion_traces.py  # generate simulator-ready JSON head motion traces
```

## Source files
We provide the source files that were used to generate the simulator inputs. These files are:
- MP4 video files, used in the video preprocessing pipeline to ultimately generate the JSON video manifests.
- CSV head motion trace files of users watching the videos provided above, used to generate the JSON head motion traces.

Video and user data comes from the datasets used [here](https://gitlab.com/miguelfromeror/head-motion-prediction). [Click here](https://unice-my.sharepoint.com/:f:/g/personal/quentin_guimard_unice_fr/En7YiNaiR2FDl9sWkz5hcKEBCrhjfoOsR2eOOzdiyUnHKw?e=L5UdBW) to download them.

## Running the code
### Video preprocessing pipeline
#### XML generation
This is the very first step of the video preprocessing pipeline. The Java application needs XML configuration files in order to know which videos to process, in which quality levels, which tiling layouts and which segment durations.
You can generate new XML configuration files for the preprocessing pipeline by executing the `xml_generator.py` Python file.

#### Java application (tiling and re-encoding + DASH packaging)
You can compile and execute the Java application by running the commands given in the [Getting started](#getting-started) section.
In order for the application to work properly, FFMPEG with libx265 and GPAC MP4Box need to be installed (everything is already installed in the provided Docker image).

#### JSON manifest generation
This is the final step of the video preprocessing pipeline. The files generated by the Java application are DASH-SRD tracks and manifest for each video, but the SMART360 simulator only needs simplified JSON manifests.
You can generate new JSON video manifests for the simulator by executing the `generate_json_video_manifests.py` Python file.

### JSON head motion traces generation
We also provide the source CSV files for the user head motion traces in `source/head_motion_traces/` (files to be downloaded separately, see section [Source files](#source-files)).
To generate simulator-ready JSON head motion traces, simply run `generate_json_head_motion_traces.py`.
